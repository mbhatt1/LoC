import subprocess 
import os
import sys
from Shutdown import *
import argparse
import requests
import socket

# Import APC.txt as a dictionary file : named as  apcMap
#Fabric API
#https://gist.github.com/arunoda/7790979
#sshpass
 
#["ssh","host.example.com","getent", "passwd","dev"]


def is_ssh_up(server_ip, port=22):
    try:
        test_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        test_socket.connect((server_ip, port))
    except Exception, ex:
        # not up, log reason from ex if wanted
        return False
    else:
        test_socket.close()
    return True


def APCTrigger(infofile='./input.txt'):
	apcMap ={}        
	with open("APC.txt") as f:
	    for line in f:
	        if " : " in line:
	          (key,val) = line.strip().split(" : ")
	          apcMap[key] =val

	if 'STATUS' in apcMap and apcMap['STATUS'] == 'ONLINE':
	    print("Congratulations! UPS is online")
	    print("Your Shut down program is now running")
	    #subprocess.call(['python', 'Shutdown.py'])
	    initiate_shutdown_of_servers(info=infofile)
	    print("Congratulations! The shutdown task has succeded")

	else: 
	   print ("UPS is not online. Please make sure it's online")

	return


parser = argparse.ArgumentParser('Before you start, be sure to check on your input files.\n')
parser.add_argument('-i', '--inputfile', help='File with IP, Order and delay', required=True)
args = vars(parser.parse_args())

if args['inputfile'] is None:
	print 'No input file provided. Please provide the input file for servers.'
else:
	print args['inputfile']
	didIImport()
	APCTrigger(infofile=args['inputfile'])
