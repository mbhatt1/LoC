import subprocess 
import sys 
import time 

def initiate_shutdown_of_servers(info='./input.txt'): 
  f = open(info, 'r') 
  lines = f.readlines() 
  lines = [x.split(',') for x in lines] 
  print lines

  for i in lines: 
      HOST="%s" %i[1].strip() 
   
      COMMAND="shutdown -h now" 
   
      ssh = subprocess.Popen(["ssh", "%s" % HOST, COMMAND], 
                         shell=False, 
                         stdout=subprocess.PIPE, 
                         stderr=subprocess.PIPE) 
      result = ssh.stdout.readlines() 
      if result == []: 
          error = ssh.stderr.readlines() 
          print >>sys.stderr, "ERROR: %s" % error 
      else: 
          print result 
      print 'sleeping for x secs: '+ str(i[2].strip()) + '\n' 
      time.sleep(int(i[2].strip())) 
  f.close()

def didIImport():
  print 'Yay!\n'
