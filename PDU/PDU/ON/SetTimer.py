import os
import subprocess
import sys

#This script only sets the timer for the delay, it doesn't turn on. 
#To turn on, you need to run another script
# Delimiter is two spaces
# put first column  is  host
# second column is  the outlets
# Third column is the time delay in seconds


def set_on_timer():
    f = open('SetTimerOutlet.txt', 'r') 
    lines = f.readlines() 
    lines = [x.split('  ') for x in lines] 
    for i in lines: 
        HOST="%s" %i[0].strip()
        OUTLETS = "%s" %i[1].strip()
        DELAY = "%s" %i[2].strip()
     
        COMMAND=" olOnDelay " + OUTLETS + ' ' +DELAY

        print (COMMAND)

        ssh = subprocess.Popen(["ssh", "%s" % HOST, COMMAND], 
                           shell=False, 
                           stdout=subprocess.PIPE, 
                           stderr=subprocess.PIPE) 
        result = ssh.stdout.readlines() 
        if result == []: 
            error = ssh.stderr.readlines() 
            print >>sys.stderr, "ERROR: %s" % error 
        else: 
            print ("Your schedule was successfull")
       
        
    f.close()

def didIImport():
  print 'Yay!\n'