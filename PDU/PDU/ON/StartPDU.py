import os
import subprocess
import sys

#This scripts turns on the outlets.
# Delimiter is two spaces
# put first column  as host
# second column as the outlets


def start_turning_on_pdu():
  f = open('Outlets.txt', 'r') 
  lines = f.readlines() 
  lines = [x.split('  ') for x in lines] 
  for i in lines: 
      HOST="%s" %i[0].strip()
      OUTLETS = "%s" %i[1].strip()
   
      COMMAND=" olDlyOn " + OUTLETS

      print (COMMAND)
      ssh = subprocess.Popen(["ssh", "%s" % HOST, COMMAND], 
                         shell=False, 
                         stdout=subprocess.PIPE, 
                         stderr=subprocess.PIPE) 
      result = ssh.stdout.readlines() 
      if result == []: 
          error = ssh.stderr.readlines() 
          print >>sys.stderr, "ERROR: %s" % error 
      else: 
          print ("Your schedule was successfull")
     
  f.close()
 
 
def didIImport():
  print 'Yay! Inside ON StartPDU\n'
